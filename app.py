import sys
from flask import Flask, render_template, request, url_for, redirect
from models import Course, RegisteredStudent
from config import Config
from flask_migrate import Migrate
from models import db


app = Flask(__name__)
app.config.from_object(Config)
db.init_app(app)
migrate = Migrate(app, db)

@app.route('/')
def welcome():
    courses = Course.query.all()
    return render_template('index.html', courses=courses)

@app.route('/add_course', methods=['POST'])
def add_course():
    if request.method == 'POST':
        course_title = request.form['course_title']
        course_number = request.form['course_number']
        course_id = request.form['course_ID']
        course = Course(id=course_id, course_number=course_number, course_title=course_title)
        db.session.add(course)
        db.session.commit()
    return redirect(url_for('welcome'))


@app.route('/register_student/<int:course_id>', methods=['POST','GET'])
def register_student(course_id):
    courses = Course.query.all()
    students = RegisteredStudent.query.all()
    for course in courses:
        if course.id == course_id:
            c = course
    if request.method == 'POST':
        student_id = request.form['student_id']
        student_name = request.form['student_name']
        student_grade = request.form['student_grade']
        student = RegisteredStudent(id=student_id, name=student_name, grade=student_grade, course_id=course_id)
        db.session.add(student)
        db.session.commit()
    return render_template('course_details.html', course=c, students=students)

@app.route('/add_student/<int:course_id>')
def add_student(course_id):
    return render_template('index.html')



if __name__ == '__main__':
    app.run()

