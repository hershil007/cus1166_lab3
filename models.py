from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Course(db.Model):
    __tablename__="course"

    id = db.Column(db.Integer, primary_key=True)
    course_number = db.Column(db.String(20))
    course_title = db.Column(db.String(30))

    registeredStudent = db.relationship("RegisteredStudent", backref="course", lazy=True)


class RegisteredStudent(db.Model):
    __tablename__="registeredStudent"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    grade = db.Column(db.Integer)

    course_id = db.Column(db.Integer, db.ForeignKey('course.id'), nullable=False)
